[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/alexmonnet/kobold) 
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=alexmonnet_kobold&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=alexmonnet_kobold)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=alexmonnet_kobold&metric=alert_status)](https://sonarcloud.io/dashboard?id=alexmonnet_kobold)

# Kobold
Kobold automates the bumping and echoing of a semantic version from several different file types such as package.json and build.gradle

# Producing a distribution
To produce an executable to distribute make sure pyinstaller is installed.
```pip3 install pyinstaller```

Once pyinstaller is installed, run the pyinstaller on the directory with the onefile and name arguments.
```pyinstaller -F -n kobold src/main.py```
