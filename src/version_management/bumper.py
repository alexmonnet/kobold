VERSION_PATCH='patch'
VERSION_MINOR='minor'
VERSION_MAJOR='major'

def get_patch_value():
    return VERSION_PATCH

def get_minor_value():
    return VERSION_MINOR

def get_major_value():
    return VERSION_MAJOR

def bump_version(old_version, version_bump_type):

    version_sections = old_version.split(".")
    version_major = int(version_sections[0])
    version_minor = int(version_sections[1])
    version_patch = int(version_sections[2])

    if version_bump_type == VERSION_PATCH:
        version_patch += 1
    elif version_bump_type == VERSION_MINOR:
        version_patch = 0
        version_minor += 1
    elif version_bump_type == VERSION_MAJOR:
        version_patch = 0
        version_minor = 0
        version_major += 1

    return '{0}.{1}.{2}'.format(version_major, version_minor, version_patch)