import argparse
import parsing.profiles as file_profiles
import parsing.parser as file_parser
import version_management.bumper as version_bumper

def action_print(args):
    file_data = args.file.read()
    version = file_parser.parse_file_data_for_version(file_data, args.profile)
    print(version)
    
def action_bump(args):
    version_file = args.file
    file_data = version_file.read()

    old_verison = file_parser.parse_file_data_for_version(file_data, args.profile)
    new_verion = version_bumper.bump_version(old_verison, args.version)
    new_file_data = file_parser.replace_version_in_file_data(file_data, args.profile, new_verion)
    
    version_file.seek(0)
    version_file.write(new_file_data)
    version_file.truncate()
    version_file.close()

    if(args.verbose):
        print("Old version: {0}".format(old_verison))
        print("New version: {0}".format(new_verion))

parser = argparse.ArgumentParser(description='Provide a language and tool agnostic way to bump verion numbers and print them from file.')

subparsers = parser.add_subparsers()
parse_bump = subparsers.add_parser('bump', help='Bump the semantic version')
parse_bump.add_argument('version', type=str, default='minor', choices=[version_bumper.get_patch_value(), version_bumper.get_minor_value(), version_bumper.get_major_value()], help='Major.Minor.Patch')
parse_bump.set_defaults(func=action_bump)

parse_print = subparsers.add_parser('print', help='Print the version')
parse_print.set_defaults(func=action_print)

parser.add_argument('profile', type=str, choices=[file_profiles.get_java_gradle_value(), file_profiles.get_js_package_value()], help='The regex profile to use on the file.')
parser.add_argument('file', type=argparse.FileType('r+'), help='a file containing the version number')
parser.add_argument('-v', '--verbose', action="store_true", help='indicate verbose output')


def main():
    args = parser.parse_args()
    args.func(args)

if __name__ == "__main__":
    main()
else: 
    pass
