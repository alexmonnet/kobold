import re
import sys
import parsing.profiles as profiles

def _get_regex_profile(profile) -> str:
    regex_string=""

    if (profile == profiles.get_java_gradle_value()):
        regex_string=profiles.get_java_gradle_regex()

    elif (profile == profiles.get_js_package_value()):
        regex_string=profiles.get_js_package_regex()

    return regex_string

def parse_file_data_for_version(file_data, profile) -> str:

    regex_string = _get_regex_profile(profile)
    matches = re.search(regex_string, file_data, flags=re.MULTILINE)
    if matches and len(matches.groups()) >= 2:
        return matches.group(2)
    else:
        print("Failed to retrieve version number")
        sys.exit()

def replace_version_in_file_data(file_data, profile, new_version):

    regex_string = _get_regex_profile(profile)
    result = re.sub(regex_string, r'\g<1>{0}\g<3>'.format(new_version), file_data, flags=re.MULTILINE)
    return result