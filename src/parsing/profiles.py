# Available Profiles
PROFILE_JS_PACKAGE='js-package'
PROFILE_JAVA_GRADLE='java-gradle'

###################################
# JavaScript package.json profile #
###################################

def get_js_package_value():
    return PROFILE_JS_PACKAGE


def get_js_package_regex():
    """
    This regex seeks to provide two capture groups:
    group 1: the version key and any spacing present
    group 2: the version number
    group 3: anything following after the version number, like a ','
    """
    return r"^(\s*\"version\":\s*\")(.*)(?=\")(\".*)$"


#######################
# Java Gradle profile #
#######################

def get_java_gradle_value():
    return PROFILE_JAVA_GRADLE


def get_java_gradle_regex():
    """
    This regex seeks to provide two capture groups:
    group 1: the version key and anything up to the version number
    group 2: the version number
    group 3: anything following after the version number
    """
    return r"^(.*applicationVersion=')(.*)(?=')(\'.*)$"